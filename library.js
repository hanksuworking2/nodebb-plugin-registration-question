"use strict";

const plugin = module.exports;
const meta = require.main.require('./src/meta');
// const User = require.main.require('./src/user');
const db = require.main.require('./src/database');

plugin.init = async function (params) {
	const { router } = params;
	const routeHelpers = require.main.require('./src/routes/helpers');
	routeHelpers.setupAdminPageRoute(router, '/admin/plugins/registration-question', renderAdmin);
};

plugin.addAdminNavigation = async function (header) {
	header.plugins.push({
		route: '/plugins/registration-question',
		icon: 'fa-tint',
		name: 'Registration Product'
	});
	return header;
};

plugin.addCaptcha = async function (params) {
	// get answers set in the ACP.
	// Since .split() is used in here, answers will always be Array.
	const answers = meta.config['registration-question:answer'].split(',').map(answer => answer.trim());

	// pass answers to templateData
	params.templateData.answers = answers;
	console.log(`pass ${answers} to template:`, params.templateData.answers)


	const question = meta.config['registration-question:question'];

	let options = '';
	for (const answer of answers) {
		options += `
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="registration-question" id="registration-question-${answer}" value="${answer}">
            <label class="form-check-label" for="registration-question-${answer}">${answer}</label>
        </div>
		`;
	}

	let captchaHtml = `<div class="card card-body">
    <strong>${question}</strong>
    <br />
    <div class="form-group">
			${options}
		</div>
	</div>`;


	const captcha = {
		label: 'Product',
		html: captchaHtml
		// html: `<div class="card card-body"><strong>${question}</strong><br />{{{ include 'plugins/registration-question/registration_question' }}}</div>`
		// html: '<div class="card card-body"><strong>' + question + '</strong><br /><input class="form-control" name="registration-question" id="registration-question" /></div>'
	};

	if (params.templateData.regFormEntry && Array.isArray(params.templateData.regFormEntry)) {
		params.templateData.regFormEntry.push(captcha);
	} else {
		params.templateData.captcha = captcha;
	}

	return params;
};

plugin.checkRegister = async function (params) {
	const username = params.userData.username
	const acpAnswers = String(meta.config['registration-question:answer']);
	const configuredAcpAnswers = acpAnswers.toLowerCase().split(',').map(answer => answer.trim());
	console.log(`Answers from user ${username}:`, params.req.body['registration-question'])

	const userAnswers = params.req.body['registration-question']
	if (userAnswers === undefined || userAnswers === '') {
		throw new Error(`Product type is required.`);
	}
	
	let registrationAnswer = ''

	// if userAnswers is array, check every entries, then store it as a combined string (with ',')
	if (Array.isArray(userAnswers)) {
		for (let answer of userAnswers) {
			// Check user's answer is included in the configured answers	
			if (!configuredAcpAnswers.includes(answer.trim().toLowerCase())) {
				throw new Error(`Wrong registration product. Supported products are: ${acpAnswers}. Your answer is ${userAnswers}.`);
			}
		}

		registrationAnswer = userAnswers.join(', ')

	} else {
		// userAnswers is a string, direcly store it in to database.
		// Check user's answer is included in the configured answers	
		if (!configuredAcpAnswers.includes(userAnswers.trim().toLowerCase())) {
			// const supportedAnswers = configuredAcpAnswers.join(', ');
			throw new Error(`Wrong registration product. Supported products are: ${acpAnswers}. Your answer is ${userAnswers}.`);
		}

		registrationAnswer = userAnswers

	}


	let key = "registration:queue:name:" + username
	await db.setObjectField(key, 'registrationAnswer', registrationAnswer)

	return params;
};


plugin.customHeader = async function (customHeaders) {
	customHeaders.headers.push({ label: 'Product Type' });
	return customHeaders;
};

plugin.getRegistrationQueue = async function (data) {
	// Get user data passed to the event
	const { users } = data;

	// Iterate through user data and add customRows for each user
	for (const user of users) {
		const customRows = await getCustomRowsForUser(user.username);

		// Add customRows to the user data
		user.customRows = customRows;
	}

	return data;
}


async function getCustomRowsForUser(username) {
	// key of MongoDB ({ _key : registration:queue:name:<username> })
	let key = "registration:queue:name:" + username

	// find the answer of the user
	const registrationAnswer = await db.getObjectField(key, 'registrationAnswer');

	// in order to fit the format of registration.tpl, return the data like the following
	return [
		{ label: 'Product Type', value: registrationAnswer },
	];
}


function renderAdmin(req, res) {
	res.render('admin/plugins/registration-question', {
		title: 'Registration Question & Answer',
	});
}