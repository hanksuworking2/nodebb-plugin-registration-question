<div class="form-group">
    <label for="registration-question">Registration Product</label>
    <select class="form-control" name="registration-question" id="registration-question">
        {{{ each ./answers}}}
            <option value="{{{ this }}}">{{{ this }}}</option>
        {{{ end }}}
    </select>
</div>
