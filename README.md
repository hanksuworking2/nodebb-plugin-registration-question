# Question and Answer plugin for the NodeBB registration form

fork from [https://github.com/psychobunny/nodebb-plugin-registration-question](https://github.com/psychobunny/nodebb-plugin-registration-question). Hank Su

A really simple plugin that adds a field to your registration page to help prevent spammers. Set your question and answer in the ACP.

## Installation

    npm install nodebb-plugin-registration-question
